﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Models;

namespace PomeloContext
{
    public class DesignTimeFactory : IDesignTimeDbContextFactory<PomeloContext>
    {
        public PomeloContext CreateDbContext(string[] args)
        {
            return new PomeloContext();
        }
    }
    public class PomeloContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("Server=localhost;Database=test;Uid=root;Pwd=Test;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddModels();
            base.OnModelCreating(modelBuilder);
        }
    }
}
