﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PostgreTest.Migrations
{
    [DbContext(typeof(Context))]
    partial class ContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("EfCoreTests.Models.BasicModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("basic");
                });

            modelBuilder.Entity("EfCoreTests.Models.Child", b =>
                {
                    b.Property<int>("ParentId");

                    b.HasKey("ParentId");

                    b.ToTable("child");
                });

            modelBuilder.Entity("EfCoreTests.Models.GrandChild", b =>
                {
                    b.Property<int>("ParentId");

                    b.Property<string>("Name");

                    b.HasKey("ParentId");

                    b.ToTable("grand_child");
                });

            modelBuilder.Entity("EfCoreTests.Models.Parent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("parent");
                });

            modelBuilder.Entity("EfCoreTests.Models.Child", b =>
                {
                    b.HasOne("EfCoreTests.Models.Parent", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EfCoreTests.Models.GrandChild", b =>
                {
                    b.HasOne("EfCoreTests.Models.Child", "Parent")
                        .WithMany("GrandChild")
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
