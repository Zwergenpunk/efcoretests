using EfCoreTests.Models;
using NUnit.Framework;
using PostgreTest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        private Context Context { get; set; }

        [SetUp]
        public void Setup()
        {
            Context = new Context();
            Context.Database.EnsureCreated();
        }

        [TearDown]
        public void TearDown()
        {
            Remove<GrandChild>();
            Remove<Child>();
            Remove<Parent>();
        }

        public void Remove<T>() where T : class
        {
            var all = Context.Set<T>().ToList();
            Context.Set<T>().RemoveRange(all);
            Context.SaveChanges();
        }

        [Test]
        public async Task ComplexSearch()
        {
            var model = new Parent
            {
                Children = new List<Child>
                {
                    new Child
                    {
                        GrandChild = new List<GrandChild>
                        {
                            new GrandChild
                            {
                                Name = "asd"
                            }
                        }
                    }
                }
            };
            await Context.AddAsync(model);
            await Context.SaveChangesAsync();

            Expression<Func<GrandChild, bool>> c = (grandChild) => grandChild.Name == "asd";

            var query = Context.Set<Parent>();
            var result = query.Where(parent => parent.Children.Any(child => child.GrandChild.AsQueryable().Any(c))).ToList();

            Assert.That(result.Count, Is.EqualTo(1));

        }
    }
}