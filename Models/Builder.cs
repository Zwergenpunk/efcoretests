﻿using EfCoreTests.Models;
using Microsoft.EntityFrameworkCore;

namespace Models
{
    public static class Builder
    {
        public static ModelBuilder AddModels(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BasicModel>().ToTable("basic");
            modelBuilder.Entity<Parent>().ToTable("parent");
            modelBuilder.Entity<Child>(entity =>
            {
                entity.ToTable("child");
                entity.HasKey(item => item.ParentId);
                entity.HasOne(item => item.Parent).WithMany(item => item.Children).HasForeignKey(item => item.ParentId);

            });
            modelBuilder.Entity<GrandChild>(entity =>
            {
                entity.ToTable("grand_child");
                entity.HasKey(item => item.ParentId);
                entity.HasOne(item => item.Parent).WithMany(item => item.GrandChild).HasForeignKey(item => item.ParentId);
            });

            return modelBuilder;
        }
    }
}
