﻿namespace EfCoreTests.Models
{
    public class GrandChild
    {
        public string Name { get; set; }
        public Child Parent { get; set; }
        public int ParentId { get; internal set; }
    }
}