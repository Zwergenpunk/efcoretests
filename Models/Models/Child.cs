﻿using System.Collections.Generic;

namespace EfCoreTests.Models
{
    public class Child
    {
        public int ParentId { get; set; }
        public Parent Parent { get; set; }
        public IEnumerable<GrandChild> GrandChild { get; set; }
    }
}