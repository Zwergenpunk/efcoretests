﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EfCoreTests.Models
{
    public class Parent
    {
        public int Id { get; set; }
        public IEnumerable<Child> Children { get; set; }
    }
}
