﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MySqlContext.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "basic",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_basic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "parent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_parent", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "child",
                columns: table => new
                {
                    ParentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_child", x => x.ParentId);
                    table.ForeignKey(
                        name: "FK_child_parent_ParentId",
                        column: x => x.ParentId,
                        principalTable: "parent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "grand_child",
                columns: table => new
                {
                    ParentId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_grand_child", x => x.ParentId);
                    table.ForeignKey(
                        name: "FK_grand_child_child_ParentId",
                        column: x => x.ParentId,
                        principalTable: "child",
                        principalColumn: "ParentId",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "basic");

            migrationBuilder.DropTable(
                name: "grand_child");

            migrationBuilder.DropTable(
                name: "child");

            migrationBuilder.DropTable(
                name: "parent");
        }
    }
}
