﻿using EfCoreTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EfCoreTests
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
           using(var context = new Context())
            {
                //await SingleInsert(context);

                //await MultiInertAsync(context);

                await SearchDynamicInGrandChild(context);
            }
        }

        private async static Task SearchDynamicInGrandChild(Context context)
        {
            var model = new Parent
            {
                Children = new List<Child>
                {
                    new Child
                    {
                        GrandChild = new List<GrandChild>
                        {
                            new GrandChild
                            {
                                Name = "asd"
                            }
                        }
                    }
                }
            };
            await context.AddAsync(model);
            await context.SaveChangesAsync();

            Expression<Func<GrandChild, bool>> c = (grandChild) => grandChild.Name == "asd";

            var query = context.Set<Parent>();
            var result = query.Where(parent => parent.Children.Any(child => child.GrandChild.AsQueryable().Any(c))).ToList();
            Console.WriteLine($"{result.Count()} parents found");
        }

        private static async Task MultiInertAsync(Context context)
        {
            var list = new List<BasicModel>();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new BasicModel());
            }
            await context.AddRangeAsync(list);
            await context.SaveChangesAsync();
        }

        private static async Task SingleInsert(Context context)
        {
            await context.AddAsync(new BasicModel());

            await context.SaveChangesAsync();
        }
    }
}
