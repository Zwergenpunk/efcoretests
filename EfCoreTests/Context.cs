﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Models;
using System;

namespace EfCoreTests
{
    public class DesignTimeFactory : IDesignTimeDbContextFactory<Context>
    {
        public Context CreateDbContext(string[] args)
        {
            return new Context();
        }
    }

    public class Context : DbContext
    {
        [Obsolete]
        public static readonly LoggerFactory MyLoggerFactory  = new LoggerFactory(new[] { new ConsoleLoggerProvider((a, type) => a == "Microsoft.EntityFrameworkCore.Database.Command", true) });
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($"Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = TestDb; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");

            //optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddModels();
            base.OnModelCreating(modelBuilder);
        }
    }
}
